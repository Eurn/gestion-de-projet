**1.0.0 Release (08/12/2021)**

Milestone: https://framagit.org/Eurn/gestion-de-projet/-/milestones/1

- feat #1 : Create navbar
- feat #2: Add images
- feat #3: Add text and button
